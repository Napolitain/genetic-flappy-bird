#! python3
# se lance par py -3 / python main_ia.py [-p 10] [-single]

# import de librairies
import sys # Pour les arguments sur la console
from libs.gen import * # import de la librarie personnelle d'un sous dossier avec libs. ET un fichier __init__.py blanc dedans
from tkinter import * # GUI
import random # fonctions aleatoires

try:
	if '-pop' in sys.argv:
		POP = int(sys.argv[sys.argv.index('-pop')+1]) # nombres d'oiseaux par population
	else:
		POP = 5
	if '-graph' in sys.argv: # librairie optionnelle pour les graphique
		import matplotlib.pyplot as plt # Pour le graphique, pour l'installer --> pip install matplotlib
		GRAPH_BOOL = True
	else:
		GRAPH_BOOL = False
except:
	POP = 5 # par défaut

# initialisation
window = Tk() # fenetre
window.resizable(width = False, height = False)
window.title("Flappy Bird")
window.iconbitmap("img/adn.ico")
wWindow = 500
hWindow = 500
window.geometry("{}x{}".format(wWindow, hWindow))

# constantes et variables
FPS = IntVar()
FPS.set(50) # images par secondes
POP_NUM = 1 # numero de la population
HOLE = 87 # taille en pixels entre les deux pipes (90 bien, soluce pour ~87 minimum)
result = '' # pour les stats
bestFitness = [1] # Tableau de la meilleure habilete par population

background = Canvas(window, width = 500, height = 500, background = "#4CC", bd=0, highlightthickness=0) # fond
background.pack()

# 5 tailles d'oiseaux differents
birdImg1 = PhotoImage(file="img/bird1.png")
birdImg2 = PhotoImage(file="img/bird2.png")
birdImg3 = PhotoImage(file="img/bird3.png") # taille reference / par defaut
birdImg4 = PhotoImage(file="img/bird4.png")
birdImg5 = PhotoImage(file="img/bird5.png")

class Population: # definit une population de birds
	def __init__(self, n): # n: int
		self.birds = [Bird() for x in range(n)] # definit une liste d'objets de type Bird
		self.survivors = n # compte d'oiseaux vivants
		self.elitism = {'fitness':0, 'genome':None} # stockage du genome (Class Genome) le plus performant (en fonction du fitness)

class Bird: # definit un oiseau
	def __init__(self):
		self.X = 100 # coordonnees de depart de l'oiseau
		self.Y = 250
		self.genome = genGenome() # definit un genome (le node correspondant a la hauteur des prochains sauts par rapport au pipe du bas) --> voir gen.py
		if ((self.genome.size > 2 and self.genome.size <= 3) or '-single' in sys.argv): # definit l'image de l'oiseau en fonction de sa taille (-single force birdImg3)
			self.object = background.create_image(self.X, self.Y, image=birdImg3) # (x, y, source) ; cree une image tkinter
			self.sizeMultiplier = 1
		elif (self.genome.size > 1 and self.genome.size <= 2):
			self.object = background.create_image(self.X, self.Y, image=birdImg2)
			self.sizeMultiplier = 1*0.75
		elif (self.genome.size > 0 and self.genome.size <= 1):
			self.object = background.create_image(self.X, self.Y, image=birdImg1)
			self.sizeMultiplier = 1*0.75*0.5
		elif (self.genome.size > 3 and self.genome.size <= 4):
			self.object = background.create_image(self.X, self.Y, image=birdImg4)
			self.sizeMultiplier = 1*1.25
		elif (self.genome.size > 4):
			self.object = background.create_image(self.X, self.Y, image=birdImg5)
			self.sizeMultiplier = 1*1.25*1.25
		self.alive = True # vivant ou non
		self.fitness = 0 # habilete / score plus precis (il s'increment chaque frame)
		self.score = 0 # score de l'oiseau (nombre de pipes passes)
		self.velocity = 0 # vitesse

	def reset(self): # remet a zero les coordonnees (et peut changer son image)
		self.X = 100
		self.Y = 250
		if ((self.genome.size > 2 and self.genome.size <= 3) or '-single' in sys.argv):
			background.itemconfig(self.object, image=birdImg3) # change l'image de l'oiseau
			self.sizeMultiplier = 1*0.75*0.75
		elif (self.genome.size > 1 and self.genome.size <= 2):
			background.itemconfig(self.object, image=birdImg2)
			self.sizeMultiplier = 1*0.75
		elif (self.genome.size > 0 and self.genome.size <= 1):
			background.itemconfig(self.object, image=birdImg1)
			self.sizeMultiplier = 1
		elif (self.genome.size > 3 and self.genome.size <= 4):
			background.itemconfig(self.object, image=birdImg4)
			self.sizeMultiplier = 1*1.25
		elif (self.genome.size > 4):
			background.itemconfig(self.object, image=birdImg5)
			self.sizeMultiplier = 1*1.25*1.25
		background.coords(self.object, self.X, self.Y)
		self.alive = True
		self.fitness = 0
		self.score = 0
		self.velocity = 0

	def fly(self): # etablit une vitesse forte negative (vers le haut)
		self.velocity = -4.75

	def getBirdY(self): # hauteur de l'oiseau relative au pipeline (pipeline du bas - oiseau)
		return pipelineY + HOLE - self.Y

	def getBirdX(self): # distance de l'oiseau relative au pipeline
		return pipelineX - self.X

population = Population(POP) # definit une population de POP (int) oiseaux

pipelineX = 500 # coordonnes pour les pipes
pipelineY = 150
pipelineTop = background.create_rectangle(pipelineX, 0, pipelineX + 70, pipelineY, fill="#7B2", outline="#7B2")
pipelineBottom = background.create_rectangle(pipelineX, pipelineY + HOLE, pipelineX + 70, 500, fill="#7B2", outline="#7B2")

scoreText = background.create_text(230, 50, fill="white", font="Times 50 bold", text="0") # texte score actuel
popText = background.create_text(60, 485, fill="white", font="Times 12 bold", text="Population n° 1") # texte population actuelle

# fonctions
def restart(): # cette fonction relance le jeu quand il n'y a plus de survivants (sans recreer des canvas par exemples)
	global population, POP_NUM, bestFitness
	global pipelineX, pipelineY

	POP_NUM += 1
	background.itemconfig(popText, text="Population n° {}".format(POP_NUM)) # Affiche le nombre de pop qui ont vecu
	pipelineX = 500
	pipelineY = 150
	updateResult()

	for bird in population.birds: # reforme la population
		if (bird.fitness >= population.elitism['fitness']): # enregistre le meilleur genome (elitism)
			population.elitism['fitness'] = bird.fitness
			population.elitism['genome'] = bird.genome
		if (population.elitism['fitness'] < 100): # si le meilleur n'est pas si bon que ca on regenere
			bird.genome = genGenome()
		else: # sinon on enchaine une mutation et un crossover (avec le meilleur : elitism)
			bird.genome = bird.genome.mutate()
			bird.genome = Genome(bird.genome.hauteurNode, bird.genome.size).crossover(population.elitism['genome'])
		bird.reset() # reset (coordonnees)

	if (population.elitism['genome'] != None): # si il existe un genome enregistre pour sa performance, on choisit le meilleur genome pour deux oiseaux
		population.birds[0].genome = population.elitism['genome'] # sans mutations pour le premier pour eviter une mauvaise evolution sur le premier oiseau
		population.birds[1].genome = population.elitism['genome'].mutate() # avec mutation pour assurer une evolution quelconque sur le deuxieme oiseau

	population.survivors = POP
	background.coords(pipelineTop, pipelineX, 0, pipelineX + 70, pipelineY) # repositionne les pipelines
	background.coords(pipelineBottom, pipelineX, pipelineY + HOLE, pipelineX + 70, 500)
	background.itemconfig(scoreText, text="0")
	motion() # relance la fonction principale

def motion(): # fonction principale en charge du fonctionnement
	global pipelineX, pipelineY, population
	print([bird.fitness for bird in population.birds], population.elitism['fitness']) # affichage fitness

	if (population.survivors <= 0): # s'il n'y a pas de survivants, redemarrage
		if (GRAPH_BOOL == True):
			bestFitness.append(1) # ajoute un score a la liste des habiletes / fitness pour le graphique
			updateGraph()
		restart()
		return False
	else:
		bestFitness[-1] += 1 # incremente le score / habilete du dernier element de la liste
		if (bestFitness[-1] % 50 == 0 and GRAPH_BOOL == True): # toutes les 50 frames (si -graph), met a jour le graphique
			plt.clf()
			updateGraph()

	for bird in population.birds: # parcours tous les oiseaux
		if (bird.alive == True): # si l'oiseau est vivant
			bird.Y += bird.velocity # augmente la hauteur par la valeur vitesse
			bird.velocity += 0.25 # gravite (augmente la vitesse)
			background.coords(bird.object, 100, bird.Y)

			if (bird.X < pipelineX and (bird.X + 9) >= pipelineX): # l'oiseau a franchi l'obstacle
				bird.score += 1 # incremente le score
				background.itemconfig(scoreText, text=bird.score)

			if ((bird.X + (21*bird.sizeMultiplier)) >= pipelineX and bird.X <= (pipelineX + 70)): # collisions : l'oiseau est mort
				if ((bird.Y - (17*bird.sizeMultiplier)) <= pipelineY or (bird.Y + (17*bird.sizeMultiplier)) >= (pipelineY + HOLE)):
					bird.alive = False
					population.survivors -= 1 # decremente la population

			bird.fitness += 1

			if (bird.getBirdY() < bird.genome.hauteurNode): # IA : si l'oiseau est en dessous de son node, il saute
				bird.fly()

		else: # l'oiseau reste sur le pipe à sa mort (il perds aussi vite que les pipes en X)
			bird.X -= 5
			background.coords(bird.object, bird.X, bird.Y)

	if (pipelineX < -100): # replace le pipeline a droite du jeu (apres qu'il ait disparu sur la gauche; 100 : largeur)
		pipelineX = 500
		pipelineY = random.randint(0, 350) # max(pipelineY - 160, 0), min(pipelineY + 160, 350) pour bannir tous cas impossibles
	else:
		pipelineX -= 5 # sinon deplace vers la gauche de 5px

	background.coords(pipelineTop, pipelineX, 0, pipelineX + 70, pipelineY)
	background.coords(pipelineBottom, pipelineX, pipelineY + HOLE, pipelineX + 70, 500)

	if (10000 == max([bird.fitness for bird in population.birds])): # si le fitness maximum vaut exactement 10000, alors on enregistre les donnees dans un fichier texte
		with open('data/result.csv', 'w') as f:
			updateResult()
			f.write(result)

	window.after(int(1000/FPS.get()), motion) # boucle infinie, 1000/x img par secondes (ex: 1000/50, 1000/150 1000/500)

def updateResult(): # fonction servant a formater les donnees qu'on va ensuite ecrire dans un fichier texte (pour des stats uniquement)
	global result
	temp = {'soluces':[], 'fitness':[]}
	for bird in population.birds:
		temp['soluces'].append(int(bird.genome.hauteurNode))
		temp['fitness'].append(int(bird.fitness))
	index = temp['fitness'].index(max(temp['fitness']))
	result += '%s' % (POP_NUM) + ',' + str(temp['soluces'][index]) + ',' + str(max(temp['fitness'])) + '\n'

def updateGraph(): # Affiche le graphique
	plt.axis([1, POP_NUM, 0, max(bestFitness)+100]) # [xMin, xMax, yMin, yMax]
	plt.plot(bestFitness) # Courbe

# positions de la fenetre principale
xWin = window.winfo_x()
yWin = window.winfo_y()

# Fenetre de controle
controls = Toplevel()
wControls = 200
hControls = 160
controls.resizable(width = False, height = False)
controls.title("Flappy Bird - Controles")
# Placement de la fenetre de controle sur la gauche de la fentre principale
controls.geometry("{}x{}+{}+{}".format(wControls, hControls, xWin+wWindow+10, yWin))
controls.attributes("-toolwindow", 1)	# Enlever le bouton pour redimensionner la fenetre -> fenetre d'outil

label1 = Label(controls, text="Vitesse : ", cursor='pirate')
vitesseScale = Scale(controls, orient=HORIZONTAL, from_=1, to=1000, length=180, variable=FPS , cursor='sb_h_double_arrow') # Curseur pour modifier les FPS

label1.pack()
vitesseScale.pack()

# graph
if (GRAPH_BOOL == True):
	fig = plt.gcf()
	fig.canvas.set_window_title("Graphique - L'habilete en fonction de la population") # Titre de la fentre du graphique
	plt.axis([1, POP_NUM, 0, max(bestFitness)+100]) # [xMin, xMax, yMin, yMax]
	plt.plot(bestFitness) # Courbe
	plt.ylabel('habilete')
	plt.xlabel('n° de la population')
	plt.ion()
	plt.show()

# run
motion()
window.mainloop()

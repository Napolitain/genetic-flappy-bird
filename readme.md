## Présentation

Ce programme est une intelligence artificielle de Flappy bird codé en Python 3 (GUI avec Tkinter).  
Le programme est composé en deux parties :
- Un jeu Flappy Bird
- Un algorithme génétique

Le but de ce projet d'intelligence artificielle est de nous permettre d'apprendre les bases de la programmation d'IA.  
A la fin de ce projet l'oiseau doit être capable d'effectuer un score satisfaisant et au delà des performances humaines.

Dependances : ```py -3 -m pip install matplotlib```  
Le jeu se lance avec la commande ```py -3 main_ia.py [-pop integer] [-single] [-graph]```  
Remplacez ```py -3``` par ```python``` si vous n'avez pas le launcher.  
```-pop 10``` définit une population de 10 oiseaux (défaut : 5).  
```-single``` définit une population d'oiseaux de taille unique.  
```-graph``` active le graphe en temps réel (nécessite matplotlib).

![Image du jeu](https://gitlab.com/Napolitain/genetic-flappy-bird/raw/master/img/presentation.png)

## Sources d'inspirations
- https://github.com/potterua/flappy-bird/blob/master/game.py
- https://github.com/TsReaper/AI-Plays-FlappyBird

## Contributions
Projet mené par Maxime Boucher et Sylvain Reynaud en Terminale S au Lycée Jean Jaurès.  
Nous sommes deux jeunes passionnés par la technologie, nous sommes plutôt autonome mais vos conseils concernant l'IA sont les bienvenus ;)
